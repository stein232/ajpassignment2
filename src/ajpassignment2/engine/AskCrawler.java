package ajpassignment2.engine;

import ajpassignment2.util.HtmlRetriever;
import ajpassignment2.util.Pair;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * @author steinneuschwanstein
 */
public class AskCrawler extends SearchEngineCrawler {
    
    public AskCrawler() {
        baseUrl = "http://www.ask.com/web";
    }

    @Override
    public List<SearchResultModel> GetSearchResults(String query, int numberOfLinks) {
        List<SearchResultModel> srms = new ArrayList();
        
        int pageCounter = 1;
        
        do {
            List<Pair<String, String>> queries = new ArrayList<>();
            queries.add(new Pair<>("q", query));
            queries.add(new Pair<>("p", String.valueOf(pageCounter++)));

            // Get the raw html
            String page = HtmlRetriever.getPage(baseUrl, queries);

            // Get result items
            Pattern resultTag = Pattern.compile("<div class=\"web-result ur.*?>(.*?)</div>((<div class=\"web-result ur.*?>)|<div id=\"csaBottom\")", Pattern.DOTALL);
            Matcher matcher = resultTag.matcher(page);

            List<String> listItems = new ArrayList<>();
            if (matcher.find()) {
                do {
                    listItems.add(matcher.group(1));
                } while (matcher.find(matcher.start(1)));
            }

            // Extract links and create a 1 to 1 list of srms to have further 
            // data extractions
            Pattern linkPattern = Pattern.compile("<h2.*?><a.*?href=\"(.*?)\"", Pattern.DOTALL);
            List<SearchResultModel> currentPageSrms = listItems.stream()
                .map(linkPattern::matcher)
                .filter(linkMatcher -> linkMatcher.find())
                .map(linkMatcher -> {
                    SearchResultModel srm = new SearchResultModel();
                    srm.setUrl(null);
                    try {
                        srm.setUrl(new URL(linkMatcher.group(1)));
                    } catch (MalformedURLException mue) {
                        // dun care
                    } finally {
                        return srm;
                    }
                })
                .collect(Collectors.toList());
            
            // Extract title
            Pattern titlePattern = Pattern.compile("<h2.*?><a.*?>(.*?)</a></h2>", Pattern.DOTALL);
            for (int i = 0; i < listItems.size(); i++) {
                Matcher titleMatcher = titlePattern.matcher(listItems.get(i));
                if (!titleMatcher.find()) continue;
                String title = titleMatcher.group(1);
                
                // remove tags
                title = title.replaceAll("<.*?>","");
                
                currentPageSrms.get(i).setTitle(title);
            }
            
            // Extract caption
            Pattern captionPattern = Pattern.compile("<p class=\"web-result-description\">(.*?)</p>", Pattern.DOTALL);
            
            for (int i = 0; i < listItems.size(); i++) {
                Matcher captionMatcher = captionPattern.matcher(listItems.get(i));
                if (!captionMatcher.find()) continue;
                String caption = captionMatcher.group(1);
                
                // remove tags
                caption = caption.replaceAll("<.*?>","");
                
                currentPageSrms.get(i).setDescription(caption);
            }
            
            srms.addAll(currentPageSrms);
        } while (numberOfLinks > srms.size());
        
        srms.stream()
            .forEach(srm -> {
                srm.getSearchedBy().add("ask");
            });
        
        return srms;
    }
}
