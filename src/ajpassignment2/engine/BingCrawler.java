package ajpassignment2.engine;

import ajpassignment2.util.HtmlRetriever;
import ajpassignment2.util.Pair;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * @author steinneuschwanstein
 */
public class BingCrawler extends SearchEngineCrawler {
    
    public BingCrawler() {
        baseUrl = "https://www.bing.com/search";
    }
    
    /**
     * Returns the specified number of results from Bing.
     * @param query The query you want to search with Bing
     * @param numberOfLinks The number of links you would like regarding the 
     * query. May be more or less than specified depending on availability of 
     * the results 
     * @return Returns a list of SearchResultModel representing the results
     */
    @Override
    public List<SearchResultModel> GetSearchResults(String query, int numberOfLinks) {
        List<SearchResultModel> srms = new ArrayList();
        
        do {
            List<Pair<String, String>> queries = new ArrayList<>();
            queries.add(new Pair<>("q", query));
            queries.add(new Pair<>("first", String.valueOf(srms.size() + 1)));

            // Get the raw html
            String page = HtmlRetriever.getPage(baseUrl, queries);

            // Get result items
            Pattern resultTag = Pattern.compile("<li class=\"b_algo\".*?>(.*?)</li>((<li class=\"b_algo\".*?>)|(<li class=\"b_ans\".*?>))", Pattern.DOTALL);
            Matcher matcher = resultTag.matcher(page);

            List<String> listItems = new ArrayList<>();
            if (matcher.find()) {
                do {
                    listItems.add(matcher.group(1));
                } while (matcher.find(matcher.start(1)));
            }

            // Extract links and create a 1 to 1 list of srms to have further 
            // data extractions
            Pattern linkPattern = Pattern.compile("<a href=\"(.*?)\"", Pattern.DOTALL);
            List<SearchResultModel> currentPageSrms = listItems.stream()
                .map(linkPattern::matcher)
                .filter(linkMatcher -> linkMatcher.find())
                .map(linkMatcher -> {
                    SearchResultModel srm = new SearchResultModel();
                    srm.setUrl(null);
                    try {
                        srm.setUrl(new URL(linkMatcher.group(1)));
                    } catch (MalformedURLException mue) {
                        // dun care
                    } finally {
                        return srm;
                    }
                })
                .collect(Collectors.toList());
            
            // Extract title
            Pattern titlePattern = Pattern.compile("<h2.*?><a.*?>(.*?)</a></h2>", Pattern.DOTALL);
            for (int i = 0; i < listItems.size(); i++) {
                Matcher titleMatcher = titlePattern.matcher(listItems.get(i));
                if (!titleMatcher.find()) continue;
                String title = titleMatcher.group(1);
                
                // remove tags
                title = title.replaceAll("<.*?>","");
                
                currentPageSrms.get(i).setTitle(title);
            }
            
            // Extract caption
            Pattern captionPattern = Pattern.compile("<p.*?>(.*?)</p>", Pattern.DOTALL);
            
            for (int i = 0; i < listItems.size(); i++) {
                Matcher captionMatcher = captionPattern.matcher(listItems.get(i));
                if (!captionMatcher.find()) continue;
                String caption = captionMatcher.group(1);
                
                // remove tags
                caption = caption.replaceAll("<.*?>","");
                
                currentPageSrms.get(i).setDescription(caption);
            }
            
            srms.addAll(currentPageSrms);
        } while (numberOfLinks > srms.size());
        
        srms.stream()
            .forEach(srm -> {
                srm.getSearchedBy().add("bing");
            });
        
        return srms;
    }
    
    /**
     * Returns a list of results from the first page of results from Bing.
     * @param query The query you want to search with Bing
     * @return Returns a list of SearchResultModel representing the results
     */
    public List<SearchResultModel> GetLinks(String query) {
        return GetSearchResults(query, -1);
    }
}
