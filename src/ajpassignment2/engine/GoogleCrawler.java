package ajpassignment2.engine;

import ajpassignment2.util.HtmlRetriever;
import ajpassignment2.util.Pair;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * @author steinneuschwanstein
 */
public class GoogleCrawler extends SearchEngineCrawler {

    public GoogleCrawler() {
        //https://www.google.com.sg/search?client=safari&rls=en&q=asdf&ie=UTF-8&oe=UTF-8&gfe_rd=cr&ei=JduUV47fH9elvwT0tr-4DA
        baseUrl = "https://www.google.com.sg/search";
    }
    
    @Override
    public List<SearchResultModel> GetSearchResults(String query, int numberOfLinks) {
        List<SearchResultModel> srms = new ArrayList();
        
        do {
            List<Pair<String, String>> queries = new ArrayList<>();
            queries.add(new Pair<>("q", query));
            queries.add(new Pair<>("aqs", "chrome..69i57.350j0j1"));
            queries.add(new Pair<>("sourceid", "chrome"));
            queries.add(new Pair<>("start", String.valueOf(srms.size())));

            // Get the raw html
            String page = HtmlRetriever.getPage(baseUrl, queries);

            // Get result items
            Pattern resultTag = Pattern.compile("<div class=\"g\".*?>(.*?)</div>((<div class=\"g\".*?>)|(<hr class=\"rgsep\">))", Pattern.DOTALL);
            Matcher matcher = resultTag.matcher(page);

            List<String> listItems = new ArrayList<>();
            if (matcher.find()) {
                do {
                    listItems.add(matcher.group(1));
                } while (matcher.find(matcher.start(1)));
            }

            // Extract links and create a 1 to 1 list of srms to have further 
            // data extractions
            Pattern linkPattern = Pattern.compile("<h3.*?><a href=\"(.*?)\"", Pattern.DOTALL);
            List<SearchResultModel> currentPageSrms = listItems.stream()
                .map(linkPattern::matcher)
                .filter(linkMatcher -> linkMatcher.find())
                .map(linkMatcher -> {
                    SearchResultModel srm = new SearchResultModel();
                    srm.setUrl(null);
                    try {
                        srm.setUrl(new URL(linkMatcher.group(1)));
                    } catch (MalformedURLException mue) {
                        // dun care
                    } finally {
                        return srm;
                    }
                })
                .collect(Collectors.toList());
            
            // Extract title
            Pattern titlePattern = Pattern.compile("<h3.*?><a.*?>(.*?)</a></h3>", Pattern.DOTALL);
            for (int i = 0; i < listItems.size(); i++) {
                Matcher titleMatcher = titlePattern.matcher(listItems.get(i));
                if (!titleMatcher.find()) continue;
                String title = titleMatcher.group(1);
                
                title = title.replaceAll("<.*?>","");
                
                currentPageSrms.get(i).setTitle(title);
            }
            
            // Extract caption
            Pattern captionPattern = Pattern.compile("<span class=\"st\">(.*?)</span>", Pattern.DOTALL);
            
            for (int i = 0; i < listItems.size(); i++) {
                Matcher captionMatcher = captionPattern.matcher(listItems.get(i));
                if (!captionMatcher.find()) continue;
                String caption = captionMatcher.group(1);
                
                // remove <em></em> tag around the query
                caption = caption.replaceAll("<.*?>","");
                
                currentPageSrms.get(i).setDescription(caption);
            }
            
            srms.addAll(currentPageSrms);
        } while (numberOfLinks > srms.size());
        
        srms.stream()
            .forEach(srm -> {
                srm.getSearchedBy().add("google");
            });
        
        return srms;
    }

}
