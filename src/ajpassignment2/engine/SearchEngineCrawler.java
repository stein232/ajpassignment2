package ajpassignment2.engine;

import java.util.List;

/**
 *
 * @author steinneuschwanstein
 */
public abstract class SearchEngineCrawler {
    protected String baseUrl;
    abstract List<SearchResultModel> GetSearchResults(String query, int numberOfLinks);
}
