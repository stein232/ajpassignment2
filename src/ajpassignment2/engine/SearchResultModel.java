package ajpassignment2.engine;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author steinneuschwanstein
 */
public class SearchResultModel implements Serializable, Comparable<SearchResultModel> {
    private String title;
    private String description;
    private int searchOccurance;
    private String html;
    private URL url;
    private int rankPoints;
    private List<String> searchedBy;

    public SearchResultModel() {
        rankPoints = 0;
        searchedBy = new ArrayList<>();
    }

    public SearchResultModel(String title, String description, int searchOccurance, String html, URL url) {
        this();
        this.title = title;
        this.description = description;
        this.searchOccurance = searchOccurance;
        this.html = html;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSearchOccurance() {
        return searchOccurance;
    }

    public void setSearchOccurance(int searchOccurance) {
        this.searchOccurance = searchOccurance;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public int getRankPoints() {
        return rankPoints;
    }

    public void setRankPoints(int rankPoints) {
        this.rankPoints = rankPoints;
    }

    public List<String> getSearchedBy() {
        return searchedBy;
    }

    public void setSearchedBy(List<String> searchedBy) {
        this.searchedBy = searchedBy;
    }
    
    @Override
    public boolean equals(Object other) {
        if (this == other)
	    return true;
        if (other == null)
            return false;
        if (getClass() != other.getClass())
            return false;
        final SearchResultModel otherSrm = (SearchResultModel)other;
        if (!url.equals(otherSrm.url))
            return false;
        return true;
    }
    
    @Override
    public int hashCode() {
	final int prime = 101;
	return prime * url.hashCode();
    }
    
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
    }
    
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }

    @Override
    public int compareTo(SearchResultModel other) {
        if (other == null) throw new NullPointerException("Other SearchResultModel object cannot be null when comparing via compareTo");
        return Integer.compare(rankPoints, other.rankPoints);
    }
}
