/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajpassignment2.gui;

import ajpassignment2.engine.AskCrawler;
import ajpassignment2.engine.BingCrawler;
import ajpassignment2.engine.GoogleCrawler;
import ajpassignment2.engine.SearchResultModel;
import ajpassignment2.util.HtmlRetriever;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 *
 * @author steinneuschwanstein
 */
public class FXMLDocumentController implements Initializable {
    @FXML private TextField searchField;
    @FXML private CheckBox bingCB, googleCB, askCB, allCB;
    @FXML private TextField numberOfResultsField;
    @FXML private Slider threadSlider;
    @FXML private Label threadLbl;
    @FXML private Label timeTakenLbl;
    @FXML private Label numberOfResultsLbl;
    @FXML private ListView<SearchResultModel> resultsListView;
    
    private String cachedResultsPath;
    private Map<String, String> cachedResultsMap;
    private List<SearchResultModel> globalSrmList;
    
    @FXML
    private void searchBtnOnClick(ActionEvent event){
        try {
            search();
        } catch (InterruptedException ie) {
            System.out.println("May have been stopped before it could finish by the stop(executor)");
            ie.printStackTrace();
        }
    }
    
    @FXML
    private void clearCache() {
        File cachedResultsFolder = new File(cachedResultsPath);
        File[] files = cachedResultsFolder.listFiles();
        if(files != null) { //some JVMs return null for empty dirs
            for(File f: files) {
                f.delete();
            }
        }
        cachedResultsMap = new HashMap<>();
        writeResultsMapSer(cachedResultsMap);
    }
    
    @FXML
    public void resultsListViewOnClick(MouseEvent event) {
        if (event.getClickCount() == 2) {
            int index = resultsListView.getSelectionModel().getSelectedIndex();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("webview.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root1));

                WebviewController controller = fxmlLoader.<WebviewController>getController();
                controller.setHtml(new ArrayList<>(globalSrmList).get(index).getHtml());

                stage.show();
            } catch(Exception e) {
               e.printStackTrace();
            }
        }
    }
    
    @FXML
    private void allCheckBoxPressed(ActionEvent event) {
        if (allCB.isSelected()) {
            bingCB.setSelected(false);
            googleCB.setSelected(false);
            askCB.setSelected(false);
        }
    }
    
    @FXML
    private void checkBoxPressed(ActionEvent event) {
        if (bingCB.isSelected() && googleCB.isSelected() && askCB.isSelected()){
            allCB.setSelected(true);
            bingCB.setSelected(false);
            googleCB.setSelected(false);
            askCB.setSelected(false);
        } else {
            allCB.setSelected(false);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // Get current path of jar
            String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
            String decodedPath = URLDecoder.decode(path, "UTF-8");
            File jarFile = new File(decodedPath);
            
            // Create the path to get cached results
            cachedResultsPath = jarFile.getParent() + "/cached results";
            File cachedResultsDirectory = new File(cachedResultsPath);
            
            // Create the directories if needed
            cachedResultsDirectory.mkdirs();
            
            // Check if resultsMap.ser exists
            // if not then create an empty resultsMap.ser
            File resultsMapSer = new File(cachedResultsPath + "/resultsMap.ser");
            if (!resultsMapSer.exists()) {
                cachedResultsMap = new HashMap<>();
                writeResultsMapSer(cachedResultsMap);
            } else {
                cachedResultsMap = readResultsMapSer();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        globalSrmList = new ArrayList<>();
        // add a listener for the valueProperty to update the threadLbl whenever
        // the slider is being dragged.
        threadSlider.valueProperty().addListener((ov, prevValue, newValue) -> {
            threadLbl.textProperty().setValue(String.valueOf((int)threadSlider.getValue()));
        });
    }

    private void search() throws InterruptedException {
        String query = searchField.getText();
        int numberOfResults = Integer.parseInt(numberOfResultsField.getText());
        int numberOfThreads = (int)threadSlider.getValue();
        
        for (Map.Entry<String, String> entry : cachedResultsMap.entrySet()) {
            String key = entry.getKey().toLowerCase();
            if (key.equals(query.toLowerCase())) {
                List<SearchResultModel> cachedSrms = getCachedSrms(entry.getValue() + ".ser");
                if (cachedSrms != null) {
                    System.out.println("Loading cached results for query: " + query);
                    long cacheLoadTime = System.currentTimeMillis();
                    globalSrmList = cachedSrms;
                    timeTakenLbl.setText("Time taken: " + (System.currentTimeMillis() - cacheLoadTime) + "ms");
                    numberOfResultsLbl.setText("Number of results: " + globalSrmList.size());
                    setListView();
                    return;
                }
                break;
            }
        }
        
        List<SearchResultModel> srms = new ArrayList<>();
        BingCrawler bingCrawler = new BingCrawler();
        GoogleCrawler googleCrawler = new GoogleCrawler();
        AskCrawler askCrawler = new AskCrawler();
        
        List<Callable<List<SearchResultModel>>> callables = new ArrayList<>();
        if (allCB.isSelected()) {
            callables.add(() -> bingCrawler.GetSearchResults(query, numberOfResults));
            callables.add(() -> googleCrawler.GetSearchResults(query, numberOfResults));
            callables.add(() -> askCrawler.GetSearchResults(query, numberOfResults));
        } else {
            if (bingCB.isSelected())
                callables.add(() -> bingCrawler.GetSearchResults(query, numberOfResults));
            if (googleCB.isSelected())
                callables.add(() -> googleCrawler.GetSearchResults(query, numberOfResults));
            if (askCB.isSelected())
                callables.add(() -> askCrawler.GetSearchResults(query, numberOfResults));
        }
        
        ExecutorService searchEngineExecutor = Executors.newFixedThreadPool(3);
        
        System.out.println("Sending query request to the search engines");
        long t1 = System.currentTimeMillis();
        long totalTime = System.currentTimeMillis();
        searchEngineExecutor.invokeAll(callables)
            .stream()
            .map(future -> {
                List<SearchResultModel> srm = null; 
                try {
                    srm = future.get();
                } catch (InterruptedException | ExecutionException e) {
                    throw new IllegalStateException(e);
                } finally {
                    return srm;
                }
            })
            .filter(listOfUrls -> listOfUrls != null)
            .forEach(srms::addAll);
        
        stop(searchEngineExecutor);
        
        System.out.println("Get links: " + (System.currentTimeMillis() - t1) + "ms");
        System.out.println("Number of links: " + srms.size());
        
        System.out.println("Adding search engine rank points");
        srms = addPointsBySearchEngineOccuranceAndDistinct(srms);
        
        System.out.println("Creating html callables");
        List<Callable<SearchResultModel>> htmlCallables = srms.stream()
            .map(srm -> {
                Callable<SearchResultModel> htmlCallable = () -> {
                    System.out.println("1. Get " + srm.getUrl().toString());
                    String html = HtmlRetriever.getPage(srm.getUrl().toString());
                    if (html == null) return null;
                    System.out.println("2. Got " + srm.getUrl().toString());
                    srm.setHtml(html);
                    return srm;
                };
                return htmlCallable;
            })
            .collect(Collectors.toList());  
        
        ExecutorService htmlExecutor = Executors.newFixedThreadPool(numberOfThreads);
        
        System.out.println("Getting the htmls for the links");
        t1 = System.currentTimeMillis();
        globalSrmList = htmlExecutor.invokeAll(htmlCallables)
            .stream()
            .map(future -> {
                SearchResultModel srm = null;
                try {
                    srm = future.get();
                } catch (InterruptedException | ExecutionException e) {
                    throw new IllegalStateException(e);
                } finally {
                    return srm;
                }
            })
            .filter(srm -> srm != null)
            .collect(Collectors.toList());
        
        globalSrmList = addPointsByQueryOccurance(globalSrmList, query);
        
        Collections.sort(globalSrmList);
        Collections.reverse(globalSrmList);
        cacheSrm(query, globalSrmList);
        
        stop(htmlExecutor);
        
        System.out.println("Get htmls: " + (System.currentTimeMillis() - t1) + "ms");
        System.out.println("Number of htmls: " + globalSrmList.size());
        
        timeTakenLbl.setText("Time taken: " + (((double)System.currentTimeMillis() - totalTime)/1000) + "s");
        numberOfResultsLbl.setText("Number of results: " + globalSrmList.size());
        setListView();
    }
    
    private void stop(ExecutorService executor) {
        try {
            System.out.println("attempt to shutdown executor");
            executor.shutdown();
            executor.awaitTermination(5, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            System.err.println("tasks interrupted");
        }
        finally {
            if (!executor.isTerminated()) {
                System.err.println("cancel non-finished tasks");
            }
            executor.shutdownNow();
            System.out.println("shutdown finished");
        }
    }
    
    private void setListView() {
        ObservableList<SearchResultModel> items = FXCollections.observableArrayList();
        items.setAll(globalSrmList);
        resultsListView.setItems(items);
        resultsListView.setCellFactory((listView) -> {
            class ListViewCell extends ListCell<SearchResultModel> {
                
                @Override
                public void updateItem(SearchResultModel srm, boolean empty) {
                    super.updateItem(srm, empty);
                    if (srm != null) {
                        SearchResultCell srcc = new SearchResultCell();
                        srcc.setInfo(srm);
                        setGraphic(srcc.getVBox());
                    }
                }
            }
            
            return new ListViewCell();
        });
    }
    
    /**
     * To be used as both ranking and filtering out same urls
     * @param srms 
     */
    private List<SearchResultModel> addPointsBySearchEngineOccuranceAndDistinct(List<SearchResultModel> srms) {
        Map<SearchResultModel, Integer> seOccuranceMap = new HashMap<>();
        Map<SearchResultModel, List<String>> seOccuranceStringMap = new HashMap<>();
        
        srms.stream()
            .forEach(srm -> {
               Integer occurance = seOccuranceMap.putIfAbsent(srm, 1);
               List<String> searchedBy = seOccuranceStringMap.putIfAbsent(srm, srm.getSearchedBy());
               if (occurance != null) {
                   seOccuranceMap.put(srm, occurance+1);
                   searchedBy.add(srm.getSearchedBy().get(0));
               }
            });
        
        return srms.stream()
            .distinct()
            .map(srm -> {
                Integer occurance = seOccuranceMap.get(srm);
                List<String> searchedBy = seOccuranceStringMap.get(srm);
                srm.setRankPoints(srm.getRankPoints() + occurance * 5);
                srm.setSearchedBy(searchedBy);
                return srm;
            })
            .collect(Collectors.toList());
    }
    
    private List<SearchResultModel> addPointsByQueryOccurance(List<SearchResultModel> srms, String query) {
//        srms.stream()
//            .forEach(srm -> {
//                String html = srm.getHtml().replace("<.*?>", "");
//                
//            });

        String[] querySplit = query.split(" ");
        
        // Slightly better ranking would be to determine the number of points 
        // awarded for number of consecutive word match. E.g. query has 4 words 
        // split by space. If there is a 4 consecutive word match (whole exact match) then 
        // the srm would be award 4 points, if 3 consecutive word match then 3
        // and so on, the number of runs would be n(n+1)/2, n being the length of 
        // query split, thus the operation is exponential not nice but wutever.
        
        // Sample size is too small to factor in number of url reference ranking.
        
        // No point factoring in search engines' ranking cos that would just be 
        // using their algorithm to do ranking.
        
        // Gonna just do full query match and single words match thus number of runs 
        // is n+1

        for (SearchResultModel srm : srms) {
            int queryOccurance = 0;
            int rankPoints = 0;
            Pattern tagPattern = Pattern.compile("<.*?>");
            
            // not using String's replaceAll as the regex will be compiled 
            // for every replace. using the matcher's replaceAll would remove 
            // that inefficiency
            String html = tagPattern.matcher(srm.getHtml()).replaceAll("");
            
            Pattern fullQuery = Pattern.compile("(?i)" + Pattern.quote(query));
            Matcher fullQueryMatcher = fullQuery.matcher(html);
            while (fullQueryMatcher.find()) {
                rankPoints += querySplit.length;
                queryOccurance++;
            }
            
            for (int i = 0; i < querySplit.length; i++) {
                Pattern wordQuery = Pattern.compile("(?i)" + Pattern.quote(querySplit[i]));
                Matcher wordQueryMatcher = wordQuery.matcher(html);
                while (wordQueryMatcher.find()) {
                    rankPoints++;
                }
            }
            srm.setSearchOccurance(queryOccurance);
            srm.setRankPoints(srm.getRankPoints() + rankPoints);
        }
        
        return srms;
    }
    
    private void writeResultsMapSer(Map cachedResultsMap) {
        try {
            FileOutputStream fos = new FileOutputStream(cachedResultsPath + "/resultsMap.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(cachedResultsMap);
            oos.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    
    private Map<String, String> readResultsMapSer() {
        Map<String, String> resultsMap = null;
        try {
            FileInputStream fis = new FileInputStream(cachedResultsPath + "/resultsMap.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            resultsMap = (Map<String, String>)ois.readObject();
            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        return resultsMap;
    }
    
    private void cacheSrm(String query, List<SearchResultModel> srm) {
        String uuid = java.util.UUID.randomUUID().toString();
        try {
            FileOutputStream fos = new FileOutputStream(cachedResultsPath + "/" + uuid + ".ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(srm);
            oos.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        cachedResultsMap.put(query, uuid);
        writeResultsMapSer(cachedResultsMap);
    }
    
    private List<SearchResultModel> getCachedSrms(String fileName) {
        List<SearchResultModel> cachedSrms = null;
        try {
            FileInputStream fis = new FileInputStream(cachedResultsPath + "/" + fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            cachedSrms = (List<SearchResultModel>)ois.readObject();
            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        return cachedSrms;
    }
}
