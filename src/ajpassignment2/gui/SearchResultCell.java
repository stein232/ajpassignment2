/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajpassignment2.gui;

import ajpassignment2.engine.SearchResultModel;
import java.io.IOException;
import java.util.Arrays;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author steinneuschwanstein
 */
public class SearchResultCell {
    @FXML VBox vBox;
    @FXML Label titleLbl;
    @FXML Label queryOccuranceLbl;
    @FXML Label rankPointsLbl;
    @FXML Label searchedByLbl;
    @FXML Label captionLbl;
    
    public SearchResultCell() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SearchResultCell.fxml"));
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void setInfo(SearchResultModel srm) {
        titleLbl.setText(srm.getTitle());
        queryOccuranceLbl.setText("Query occurance: " + srm.getSearchOccurance());
        rankPointsLbl.setText("Rank points: " + srm.getRankPoints());
        searchedByLbl.setText("From: " + Arrays.toString(srm.getSearchedBy().toArray()));
        captionLbl.setText(srm.getDescription());
    }
    
    public VBox getVBox() {
        return vBox;
    }
}
