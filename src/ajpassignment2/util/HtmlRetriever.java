package ajpassignment2.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownServiceException;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author steinneuschwanstein
 */
public class HtmlRetriever {
    private static final String userAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
    private static final int averageCharacterPerPage = 150000;
    
    public static String getPage(String baseUrl, List<Pair<String, String>> queries) {
        StringBuilder fullUrlsb = new StringBuilder(baseUrl);
        try {
            // Create the queries string
            StringBuilder pageUrlsb = new StringBuilder(30);
            if (queries.size() > 0) {
                for (int i = 0; i < queries.size() - 1; i++) {
                    pageUrlsb.append(queries.get(i).getLeft())
                        .append("=")
                        .append(URLEncoder.encode(queries.get(i).getRight(), "UTF-8"))
                        .append("&");
                }
                pageUrlsb.append(queries.get(queries.size() - 1).getLeft())
                    .append("=")
                    .append(URLEncoder.encode(queries.get(queries.size() - 1).getRight(), "UTF-8"));
                fullUrlsb.append("?").append(pageUrlsb);
            }
            // Combine baseUrl and queries and convert to URL
            URL url = new URL(fullUrlsb.toString());
            
            // Get the Html from the url
            String html;
            
//            // try with https first
//            if (!url.toString().substring(0, 5).equals("https"))
//                fullUrlsb.insert(4, 's');
//            
//            html = getHttpsPage(new URL(fullUrlsb.toString()));
//            
//            if (html != null) return html;
//            
//            // try with http next
//            fullUrlsb.delete(4, 5);
//            html = getHttpPage(new URL(fullUrlsb.toString()));
            
            if (url.toString().substring(0, 5).equals("https"))
                html = getHttpsPage(new URL(fullUrlsb.toString()));
            else
                html = getHttpPage(new URL(fullUrlsb.toString()));

            return html;
        } catch (MalformedURLException e) {
            System.out.println(fullUrlsb.toString() + " failed cos malformed");
//            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(fullUrlsb.toString() + " failed cos IO probs");
//            e.printStackTrace();
        }
        
        return null;
    }
    
    public static String getPage(String url) {
        return getPage(url, new ArrayList<>());
    }
    
    private static String getHttpPage(URL url) throws IOException {
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.setRequestMethod("GET");
        // Add User-Agent to fix 403 response
        httpConnection.addRequestProperty("User-Agent", userAgent);
        
        // No reason to do output (send a body) to the page since we are just 
        // getting the html
        httpConnection.setDoOutput(false);
        httpConnection.setReadTimeout(2000);
        
        StringBuilder sb = new StringBuilder(averageCharacterPerPage);
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
        }
        
        if (sb.toString().equals("")) {
            System.out.println("Empty response on " + url.toString());
            return null;
        }
        return sb.toString();
    }
    
    private static String getHttpsPage(URL url) throws IOException {
        HttpsURLConnection httpsConnection = (HttpsURLConnection) url.openConnection();
        httpsConnection.setRequestMethod("GET");
        // Add User-Agent to fix 403 response
        httpsConnection.addRequestProperty("User-Agent", userAgent);
        
        // No reason to do output (send a body) to the page since we are just 
        // getting the html
        httpsConnection.setDoOutput(false);
        httpsConnection.setReadTimeout(5000);
        
        StringBuilder sb = new StringBuilder(averageCharacterPerPage);
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
        } catch (UnknownServiceException use) {
            System.out.println("USE occured");
//            use.printStackTrace();
        }
        
        if (sb.toString().equals("")) {
            System.out.println("Empty response on " + url.toString());
            return null;
        }
        return sb.toString();
    }
}
