package ajpassignment2.util;

import java.io.IOException;
import java.io.Serializable;

/**
 *
 * @author steinneuschwanstein
 * @param <L> Datatype of the left object
 * @param <R> Datatype of the right object
 */
public class Pair<L, R> implements Serializable {
    private static final long serialVersionUID = 8282748383747273647L;
    
    private L left;
    private R right;

    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public L getLeft() {
        return left;
    }

    public void setLeft(L left) {
        this.left = left;
    }

    public R getRight() {
        return right;
    }

    public void setRight(R right) {
        this.right = right;
    }
    
    @Override
    public boolean equals(Object other) {
        if (this == other)
	    return true;
        if (other == null)
            return false;
        if (getClass() != other.getClass())
            return false;
        final Pair otherPair = (Pair)other;
        if (left.getClass() != otherPair.left.getClass())
            return false;
        if (right.getClass() != otherPair.right.getClass())
            return false;
        if (left == null) {
            if (otherPair.left != null)
                return false;
        }
        if (right == null) {
            if (otherPair.right != null)
                return false;
        }
        if (!left.equals(otherPair.left))
            return false;
        if (!right.equals(otherPair.right))
            return false;
        return true;
    }
    
    @Override
    public int hashCode() {
	final int prime = 101;
	return prime * left.hashCode() + prime * right.hashCode();
    }
    
    @Override
    public String toString() {
        return left.toString() + ": " + right.toString();
    }
    
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
    }
    
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }
}
